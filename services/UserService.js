const users = require('../models/users')
const context = require('express-http-context')
const Sentry = require("@sentry/node");
const request = require('request');
const { Types } = require('mongoose')

const GetUserOrCreate = async (user) => {
    try {
        let dbUser = await users.findOne({ email: user.email });
        if (dbUser) return dbUser
        dbUser = await users.create({
            name: user.name,
            email: user.email,
            googleId: user.googleId,
            memberOf: []
        })
        return dbUser
    } catch (er) {
        console.log(er)
        throw er
    }
}

const SendNotification = async (targets, content) => {
    try {
        const emails = targets.map(user => user.email)
        const expoTokens = await users.find({
            'email': { $in: emails },
            "expoToken": /ExponentPushToken/i
        }, 'expoToken')

        for (const doc of expoTokens) {
            request.post({
                headers: {
                    host: "exp.host",
                    accept: "application/json",
                    "accept-encoding": "gzip, deflate",
                    "content-type": "application/json"
                },
                url: 'https://exp.host/--/api/v2/push/send',
                json: true,
                body: {
                    "to": doc.expoToken,
                    "title": "Splitit",
                    "body": content
                }
            }, function (error, response, body) {
                if (error)
                    Sentry.captureException(error,
                        {
                            extra: {
                                user: targets,
                                content: content,
                                response: response
                            }
                        })
            });
        }
    } catch (er) {
        Sentry.captureException(er, { extra: { user: targets, content: content } })
    }
}

const getUserByEmail = async (email) => {
    try {
        let dbUser = await users.findOne({ email: email })
        return dbUser
    } catch (er) {
        console.log(er)
        throw er
    }
}

const RegisterUser = async (expoId) => {
    const currentUser = context.get('user');
    try {
        const usersWithToken = await users.find({
            expoToken: expoId,
            email: { $ne: currentUser.email }
        })

        const saveTasks = []
        for (user of usersWithToken) {
            user.expoToken = null
            saveTasks.push(user.save())
        }
        await Promise.all(saveTasks)
        let dbUser = await GetUserOrCreate(currentUser)
        dbUser.expoToken = expoId;
        await dbUser.save()
    } catch (er) {
        Sentry.setTag('Method', 'RegisterUser')
        Sentry.captureException(er, {
            extra: {
                ExpoToken: expoId,
                user: currentUser
            }
        })
        throw new Error('Error al registrar el usuario')
    }
}

module.exports = {
    GetUserOrCreate,
    getUserByEmail,
    SendNotification,
    RegisterUser
}