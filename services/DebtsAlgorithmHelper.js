function round(n) {
    return Math.round(n * 100) / 100
}

exports.calculateTotals = (group) => {
    const totals = {};
    for (var member of group.members) {
        totals[member._id] = { member: member, total: 0 }
    }

    for (var expense of group.expenses) {
        if (expense.consumeBy.length == 0)
            throw Error(`Expense must be consume by at lest 1 member (exp. ${expense._id}, gr. ${group._id})`)

        let perCapita = (expense.amount / expense.consumeBy.length)

        //Quien pago
        totals[expense.paidBy._id].total += expense.amount

        for (var consumer of expense.consumeBy) {
            if (totals[consumer._id] == undefined)
                throw Error(`Consumer (${consumer._id}) is not in member list (${group._id})`)

            totals[consumer._id].total -= perCapita
        }
    }

    if (group.payments) {
        for (var payment of group.payments) {
            var currentSourceTotal = totals[payment.source._id].total
            totals[payment.source._id].total = currentSourceTotal + payment.amount
            var currentReceiverTotal = totals[payment.receiver._id].total
            totals[payment.receiver._id].total = currentReceiverTotal - payment.amount
        }
    }

    var result = [];
    for (var id in totals) {
        totals[id].total = round(totals[id].total)
        result.push(totals[id])
    }

    return result
}

exports.calculateTransactions = (totals) => {
    var deudores = [] //personas con deudas
    var acreedores = [] //personas con deudas a favor

    for (var item of totals) {
        //deudor
        if (item.total < 0) {
            deudores.push({ member: item.member, amount: item.total })
        }
        if (item.total > 0) {
            acreedores.push({ member: item.member, amount: item.total })
        }
    }

    var transactions = []
    for (i = 0; i < deudores.length; i++) {
        var currentDebtor = deudores[i].member
        var currentDebt = -(deudores[i].amount)
        currentDebt = Math.round(currentDebt * 100) / 100

        while (currentDebt > 0.02) {
            var currentCreditor = acreedores[0]

            if (currentCreditor.amount > currentDebt) {
                transactions.push({ debtor: currentDebtor, creditor: currentCreditor.member, amount: currentDebt })
                currentCreditor.amount -= currentDebt
                currentDebt = 0
            }

            if (currentCreditor.amount == currentDebt) {
                transactions.push({ debtor: currentDebtor, creditor: currentCreditor.member, amount: currentDebt })
                currentCreditor.amount -= currentDebt
                currentDebt = 0
                acreedores.shift()
            }

            if (currentCreditor.amount < currentDebt) {
                const rounded = round(currentCreditor.amount)
                transactions.push({ debtor: currentDebtor, creditor: currentCreditor.member, amount: rounded })
                currentDebt -= rounded
                currentCreditor.amount = 0
                acreedores.shift()
            }

        }

    }

    return transactions
}