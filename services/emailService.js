const nodemailer = require("nodemailer");
const Sentry = require("@sentry/node");
const fsPromises = require('fs').promises;
const config = require('../config');

module.exports = {
    sendInvitation: async (email) => {
        if (!config.email.enabled)
            return
        try {
            const emailContent = await fsPromises.readFile('./templates/invitationEmail.html')
            const emailOption = {
                from: config.email.account,
                to: email,
                subject: "HELLO! JOIN US ✔",
                html: emailContent.toString(),
            }

            const transporter = nodemailer.createTransport({
                host: config.email.host,
                port: config.email.port,
                secure: true,
                auth: {
                    user: config.email.account,
                    pass: config.email.password,
                }
            })
            transporter.sendMail(emailOption)
        } catch (err) {
            Sentry.captureException(err)
            throw new Error("Error getting group")
        }
    }
}