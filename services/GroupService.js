const context = require('express-http-context')
const groupsModel = require('../models/groups')
const UserService = require('./UserService')
const emailService = require('./emailService')
const attachmentModel = require('../models/attachment')
const Sentry = require("@sentry/node");
const DebtsAlgorithmHelper = require('./DebtsAlgorithmHelper')
const notificationsMessages = require('../templates/notificationMessages')

function updateSummary(group) {
    var totals = []
    var resume = []
    try {
        totals = DebtsAlgorithmHelper.calculateTotals(group)
        resume = DebtsAlgorithmHelper.calculateTransactions(totals)
        group.summary = resume
    } catch (err) {
        Sentry.setExtras({
            'group': group,
            'total': totals,
            'summary': resume
        })
        Sentry.captureException(err)
        return []
    }
}

const inviteToGroup = async (members, group) => {
    for (let i = 0; i < members.length; i++) {
        let member = members[i]
        if (!member.email)
            continue

        const user = await UserService.getUserByEmail(member.email)
        if (!user) {
            await emailService.sendInvitation(member.email)
            continue;
        }

        member.name = user.name
        member.userId = user._id
        user.memberOf.push({ id: group._id, name: group.name })
        await user.save()
    }
    await group.save()
}

const reInviteToGroup = async (membersNonDeleted, currentGroup) => {

    for (let i = 0; i < membersNonDeleted.length; i++) {
        let member = membersNonDeleted[i]
        if (!member.email)
            continue

        const user = await UserService.getUserByEmail(member.email)
        if (!user)
            continue;

        const indexGroup = user.memberOf.findIndex(group => (JSON.stringify(group.id) === JSON.stringify(currentGroup._id)))
        user.memberOf[indexGroup].name = currentGroup.name
        await user.save()
    }
}

async function sendNotification(users, content) {
    const currentUser = context.get('user');
    const currentEmail = currentUser.email

    const usersToNotify = users.filter(x =>
        x.email != undefined
        && x.email != ''
        && x.email != currentEmail)

    UserService.SendNotification(usersToNotify, content)
}

module.exports = {
    groupExists: async (groupId) => {
        try {
            const currentUser = context.get('user');
            const user = await UserService.GetUserOrCreate(currentUser)
            return (user.memberOf.find(x => x.id == groupId) != undefined)
        } catch (err) {
            console.log("[GroupService] Error retriving user groups")
            console.log(err)
            Sentry.captureException(err)
            return false
        }
    },

    groupIsArchived: async (groupId) => {
        try{
            const currentUser = context.get('user');
            const user = await UserService.GetUserOrCreate(currentUser)
            return (user.memberOf.find(group => group.id == groupId).archived == true)
        }catch(err){
            console.log("[GroupService] Error retriving user groups")
            console.log(err)
            Sentry.captureException(err)
            return false
        }
    },

    groupNameExists: async (name) => {
        try {
            const currentUser = context.get('user');
            const user = await UserService.GetUserOrCreate(currentUser)
            return (user.memberOf.find(x => x.name == name) != undefined)
        } catch (err) {
            console.log("[GroupService] Error retriving user groups")
            console.log(err)
            Sentry.captureException(err)
            return false
        }
    },

    createGroup: async (group) => {
        try {
            const newGroup = new groupsModel({
                name: group.name,
                description: group.description,
                idAdmin: group.idAdmin,
                expenses: group.expenses,
                members: group.members
            })
            const currentUser = context.get('user');
            const user = await UserService.GetUserOrCreate(currentUser)
            //Agregar el usuario actual a la lista de miembros
            newGroup.members.push({ userId: user._id, name: user.name, email: user.email })

            await newGroup.save();
            await inviteToGroup(newGroup.members, newGroup)

            await sendNotification(newGroup.members, notificationsMessages.NewGroup)

            return { newGroup }
        } catch (err) {
            console.log("[GroupService] Error creating group")
            console.log(err)
            Sentry.captureException(err)
            throw new Error("Error creating group")
        }

    },

    deleteAnExpense: async(groupId, expId) => {
        try {
            var dbGroup = await groupsModel.findById(groupId)
            var dbExp = dbGroup.expenses.filter(x => x._id != expId)
            dbGroup.expenses = dbExp

            updateSummary(dbGroup)

            await dbGroup.save()

        }catch(err){
            console.log("[GroupService] Error")
            console.log(err)
            Sentry.captureException(err)
            throw new Error("Error deleting expense")
        }
        
    },

    editGroup: async(dtoGroup) => {
        try{
            var dbGroup = await groupsModel.findById(dtoGroup.id)

            dbGroup.description = dtoGroup.description
            dbGroup.idAdmin = dtoGroup.idAdmin
            //In Dto not in db
            var newMembers = dtoGroup.members.filter(dto => !dbGroup.members.some(db => db.name == dto.name && db.email == dto.email))
            //Rescato el usuario Logueado, de lo contrario es borrado
            const currentUser = dbGroup.members.pop()
    
            //In db and in Dto
            var notDeleted = dbGroup.members.filter(db => dtoGroup.members.some(dto => db.name == dto.name && db.email == dto.email));

            //in future we may want to edit instead of delete an create anew
            if (dbGroup.name != dtoGroup.name) {
                dbGroup.name = dtoGroup.name
                await reInviteToGroup([...notDeleted, currentUser], dbGroup)
            }

            dbGroup.members = [...newMembers]
            await inviteToGroup(dbGroup.members, dbGroup)
            dbGroup.members.push(...notDeleted)
            dbGroup.members.push(currentUser)
            const saved = await dbGroup.save()

            return saved
        }catch(err){
            console.log("[GroupService] Error")
            console.log(err)
            Sentry.captureException(err)
            throw new Error("Error editing group")
        }
        
    },

    getGroups: async()=>{
        try {
            const currentUser = context.get('user');
            const user = await UserService.GetUserOrCreate(currentUser)
            return user.memberOf;
        } catch (err) {
            console.log("[GroupService] Error")
            console.log(err)
            throw new Error("Error getting group")
        }
    },

    getGroup: async (id) => {
        try {
            const group = await groupsModel.findById(id)
            return group;
        } catch (err) {
            console.log("[GroupService] Error")
            console.log(err)
            Sentry.captureException(err)
            throw new Error("Error getting group")
        }
    },

    createExpense: async (expense, idGroup) => {
        try {
            const currentGroup = await groupsModel.findById(idGroup);
            const length = currentGroup.expenses.push(expense)

            updateSummary(currentGroup)

            currentGroup.save()

            sendNotification([...expense.consumeBy, expense.paidBy], notificationsMessages.NewExpense)

            return currentGroup.expenses[length - 1]
        } catch (err) {
            console.log("[GroupService] Error")
            console.log(err)
            Sentry.captureException(err, { extra: { expense, idGroup } })
            throw new Error("Error creating expense")
        }
    },

    createPayment: async(payment, idGroup)=>{
        try {
            const currentGroup = await groupsModel.findById(idGroup);
            const length = currentGroup.payments.push(payment) 

            updateSummary(currentGroup)

            currentGroup.save()

            sendNotification([payment.source, payment.receiver], notificationsMessages.NewPayment)

            return currentGroup.payments[length - 1]
        } catch (err) {
            console.log("[GroupService] Error")
            console.log(err)
            Sentry.captureException(err)
            throw new Error("Error creating payment")
        }
    },

    createAttachment: async (attachment, idGroup, idExpense) => {
        try {
            const newAttachment = await new attachmentModel({ img: attachment }).save()
            const dbGroup = await groupsModel.findById(idGroup)
            const expenseIndex = dbGroup.expenses.findIndex(x => x._id == idExpense)
            dbGroup.expenses[expenseIndex].attachments.push(newAttachment._id)
            dbGroup.save()
            return {
                expense: dbGroup.expenses[expenseIndex],
                attachmentId: newAttachment._id
            }
        } catch (error) {
            console.log("[GroupService] Error")
            console.log(err)
            Sentry.captureException(err)
            throw new Error("Error saving attachment")
        }
    },
    archiveGroup: async (groupId) => {
        try {
            const currentUser = context.get('user');
            const user = await UserService.GetUserOrCreate(currentUser)
    
            for (let gr of user.memberOf ){
                if (gr.id == groupId){
                    gr.archived = true
                    break
                }
            }
            await user.save()
        }catch(err){
            console.log("[UserService] Error")
            console.log(err)
            Sentry.captureException(err)
            throw new Error("Error archiving group")
        }
        
    }
}