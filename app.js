const express = require ('express');
const httpContext = require('express-http-context');
const mongoose = require('mongoose');
const morgan= require('morgan');
const Sentry = require("@sentry/node");
const Tracing = require("@sentry/tracing");
const routes = require('./routes');
const createConnectionString = require('./config/DBhelper')
const AuthMiddleware = require('./middlewares/AuthenticationMiddleware');
const config = require('./config');

const app = express();

if (config.sentry.enable) {
  Sentry.init({
    dsn: config.sentry.dns,
    environment: config.name,
    integrations: [
      // enable HTTP calls tracing
      new Sentry.Integrations.Http({ tracing: true }),
      // enable Express.js middleware tracing
      new Tracing.Integrations.Express({ app })
    ],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });
}

// RequestHandler creates a separate execution context using domains, so that every
// transaction/span/breadcrumb is attached to its own Hub instance
app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());


app.use(express.json());  
app.use(express.urlencoded({ extended: true }))
app.use(morgan('dev'));

app.use(httpContext.middleware);

app.use(AuthMiddleware())
app.use('/', routes);

// The error handler must be before any other error middleware and after all controllers
app.use(Sentry.Handlers.errorHandler());

//moongose connection
mongoose.connect(createConnectionString(config.database.user, config.database.password, config.database.host, config.database.name), { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Database Online')
  const port = config.app.port;
  app.listen(port,() =>
  {
    console.log("Running on " + config.name + " configuration");
    console.log('API server: ' + port);
  });
});

module.exports = app;