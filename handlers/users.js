const service = require('../services/UserService')
const { check } = require('express-validator');

module.exports = {
    registerUserValidator: [check('expo').notEmpty()],
    register: async (req, res) => {
        try {
            await service.RegisterUser(req.body.expo)
            res.sendStatus(200)
        } catch (er) {
            res.sendStatus(500)
        }
    }
}