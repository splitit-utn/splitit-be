const service = require('../services/GroupService')
const { check, param } = require('express-validator');
const Sentry = require("@sentry/node");

const nameValidator =  async( name = '') =>{
    if (await service.groupNameExists(name)) {
        throw new Error("Group already exists")
    }
}

const groupExistsValidator = async(id) => {
    if (!await service.groupExists(id))
        throw new Error("Group doesn't exists")
}

const groupIsArchivedValidator = async(groupId) => {
    if (await service.groupIsArchived(groupId)){
        throw new Error("Group is already archived")
    }
}

const duplicatedMembersValidator = (members) => {
    let names = [];
    members.forEach(x => {
        if(names.includes(x.name))
            throw new Error("Member name duplicated: " + x.name)
        names.push(x.name)
    });
    return true
}

const membersExistValidator = async(members, {req})=>{
    const dbGroup = await service.getGroup(req.params.groupId);
    const dbMembers = dbGroup.members;
    if (Array.isArray(members)){
        members.forEach(member=>{
            if (!dbMembers.some(dbMember => member._id == dbMember._id)) {
                throw new Error(member.name + " isn't included in the current members")
            }
        })
    } else {
        if (!dbMembers.some(dbMember => members._id == dbMember._id)) {
            throw new Error(members.name + " isn't included in the current members")
        }
    }
}

const expenseExistValidator = async (expenseId, { req }) => {
    const currentGroup = await service.getGroup(req.params.groupId);
    if (!currentGroup.expenses.some(x => expenseId == x._id)) {
        throw new Error("Expense " + expenseId + " doesn't exists")
    }
    return true
}

const removeExpenseValidator = [
    param('expenseId').isMongoId().custom(expenseExistValidator),
    param('groupId').isMongoId().custom(groupExistsValidator)
]

const createExpenseValidator = [
    check('description').trim().notEmpty(),
    check('amount').notEmpty().isFloat({min:0}),
    check('date').notEmpty().isISO8601().toDate(),
    check('paidBy').custom(membersExistValidator),
    check('consumeBy').custom(membersExistValidator)
]

const createPaymentValidator = [
    param('groupId').isMongoId().custom(groupExistsValidator),
    check('amount').notEmpty().isFloat({min:0}),
    check('date').notEmpty().isISO8601().toDate(),
    check('source._id').notEmpty().isMongoId(),
    check('source.name').notEmpty(),
    check('source').custom(membersExistValidator),
    check('receiver._id').notEmpty().isMongoId(),
    check('receiver.name').notEmpty(),
    check('receiver').custom(membersExistValidator)
]

const baseGroupValidator = [
    check('name').trim().notEmpty(),
    check('description').trim().notEmpty(),
    check('idAdmin').notEmpty().isNumeric(),
    check('members').notEmpty(),
    check('members.*.name').trim().notEmpty(),
    check('members.*.email').exists().trim().isEmail().optional({ checkFalsy: true }),
    check('members').custom(duplicatedMembersValidator)
]

const createAttachmentValidations = [
    check('img').notEmpty().isBase64(),
    param('groupId').isMongoId().custom(groupExistsValidator),
    param('expenseId').isMongoId().custom(expenseExistValidator)
]

const archiveGroupValidator = [
    param('groupId').isMongoId().
    custom(groupExistsValidator).
    custom(groupIsArchivedValidator)
]

module.exports = {
    getGroups: async(req,res) =>{
        try {
            res.json(await service.getGroups())
        } catch (err) {
            console.log(`[Handler] Error`)
            console.log(err)
            res.sendStatus(500)   
        }
    },

    getGroupValidations: [param('groupId').isMongoId(), param('groupId').custom(groupExistsValidator)],
    getGroup: async (req, res) => {
        try {
            var id = req.params.groupId
            var group = await service.getGroup(id)
            res.json(group)
        } catch (err) {
            console.log(`[Handler] Error`)
            console.log(err)
            res.sendStatus(500)
        }
    },

    createGroupValidations: [check('name').custom(nameValidator), ...baseGroupValidator],

    createGroup: async (req, res) => {
        //Obtener todos los datos necesarios para la transaccion
        try {

            const group = {
                name: req.body.name,
                description: req.body.description,
                members: req.body.members,
                idAdmin: req.body.idAdmin,
            }

            const createdGroup = await service.createGroup(group);

            //Crear respuesta
            const reply = {
                success: true,
                group: createdGroup,
            }
            res.json(reply);

        } catch (err) {
            console.log(`[Handler] Error creating group`)
            console.log(err)
            Sentry.captureException(err)
            res.sendStatus(500)
        }

    },

    editGroupValidations: [param('groupId').custom(groupExistsValidator), ...baseGroupValidator],
    editGroup: async(req, res) => {
        try{
            var group = req.body
            group.id = req.params.groupId

            let saved = await service.editGroup(group);

            res.send(saved)
        }catch(err){
            console.log(`[Handler] Error`)
            console.log(err)
            Sentry.captureException(err)
            res.sendStatus(500)   
        }
    },

    createExpenseValidator,
    createExpense: async(req, res) =>{
        try {
            const expenseReq = {
                description: req.body.description,
                amount: req.body.amount,
                date: req.body.date,
                paidBy: req.body.paidBy,
                consumeBy: req.body.consumeBy
            }
            const expense = await service.createExpense(expenseReq, req.params.groupId)
            res.send({
                success: true,
                expense: expense
            })
        } catch (err) {
            console.log(`[Handler] Error`)
            console.log(err)
            Sentry.captureException(err)
            res.sendStatus(500)   
        }
    },

    createAttachmentValidations,
    createAttachment: async (req, res) => {
        try {
            const { expense, attachmentId } = await service.createAttachment(req.body.img, req.params.groupId, req.params.expenseId);
            res.send({
                success: true,
                expense: expense,
                attachmentId: attachmentId
            })
        } catch (error) {
            console.log(`[Handler] Error`)
            console.log(err)
            Sentry.captureException(err)
            res.sendStatus(500)
        }
    },
    removeExpenseValidator,
    deleteExpense: async(req, res) => {
        try{
            await service.deleteAnExpense(req.params.groupId, req.params.expenseId);
            res.send({msg:"Successfull expense remove", success:true})
        }catch(err){
            console.log(`[Handler] Error`)
            console.log(err)
            Sentry.captureException(err)
            res.sendStatus(500)   
        }
    },

    createPaymentValidator,
    createPayment: async(req, res) =>{
        try {
            const payment = {
                description: req.body.description,
                amount: req.body.amount,
                date: req.body.date,
                source: req.body.source,
                receiver: req.body.receiver
            }
            const dbPayment = await service.createPayment(payment,req.params.groupId)
            res.send({
                success:true, 
                payment: dbPayment
            })
        } catch (err) {
            console.log(`[Handler] Error`)
            console.log(err)
            Sentry.captureException(err)
            res.sendStatus(500)   
        }
    },

    archiveGroupValidator,
    archiveGroup: async (req, res) => {
        try{
            const groupId = req.params.groupId
            await service.archiveGroup(groupId)
            res.send({
                success: true
            })
        }catch(err){
            console.log(`[Handler] Error`)
            console.log(err)
            Sentry.captureException(err)
            res.sendStatus(500) 
        }
    }
}