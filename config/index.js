const env = process.env.NODE_ENV || 'local';
const default_sentry = 'https://5d3a180d9b2a492ca5303729d733fa40@o605778.ingest.sentry.io/5755346'

const local = {
  name: "local",
  app: {
    port: parseInt(process.env.PORT) || 3000
  },
  database: {
    host: process.env.APP_DB_HOST || "splitit-mongo-cluster.co3vz.mongodb.net",
    name: process.env.APP_DB_NAME || "splitit-db-dev",
    user: process.env.APP_DB_USER || "admin",
    password: process.env.APP_DB_PASSWORD || "adminpass"
  },
  email: {
    enabled: false,
    account: "",
    password: "",
    host: "",
    port: 0,
  },
  sentry: {
    dns: default_sentry,
    debug: true,
    enable: false
  }
}

const dev = {
  name: "dev",
  app: {
    port: parseInt(process.env.PORT) || 3000
  },
  database: {
    host: process.env.APP_DB_HOST || "splitit-mongo-cluster.co3vz.mongodb.net",
    name: process.env.APP_DB_NAME || "splitit-db-dev",
    user: process.env.APP_DB_USER || "admin",
    password: process.env.APP_DB_PASSWORD || "adminpass"
  },
  email: {
    enabled: process.env.APP_EMAIL_ENABLED || false,
    account: process.env.APP_EMAIL_ACCOUNT,
    password: process.env.APP_EMAIL_PASSWORD,
    host: process.env.APP_EMAIL_HOST,
    port: process.env.APP_EMAIL_PORT,
  },
  sentry: {
    dns: process.env.SENTRY_DSN || default_sentry,
    debug: true,
    enable: true
  }
}

const production = {
  name: 'production',
  app: {
    port: parseInt(process.env.PORT)
  },
  database: {
    host: process.env.APP_DB_HOST,
    name: process.env.APP_DB_NAME,
    user: process.env.APP_DB_USER,
    password: process.env.APP_DB_PASSWORD
  },
  email: {
    enabled: process.env.APP_EMAIL_ENABLED || false,
    account: process.env.APP_EMAIL_ACCOUNT,
    password: process.env.APP_EMAIL_PASSWORD,
    host: process.env.APP_EMAIL_HOST,
    port: process.env.APP_EMAIL_PORT,
  },
  sentry: {
    dns: process.env.SENTRY_DSN || default_sentry,
    debug: false,
    enable: true
  }
}

const configs = {
  local,
  dev,
  production
}

module.exports = configs[env]

