
const createConnectionString = (username, password, host, databaseName) =>
{
    return `mongodb+srv://${username}:${password}@${host}/${databaseName}`;
}

module.exports = createConnectionString;