const express = require('express');
const router = express.Router();
const handler = require('../handlers/groups')
const validator = require('../helpers/validator')

function validate(validations) {
    return [...validations, validator]
}

router.get("/", handler.getGroups);
router.get("/:groupId", validate(handler.getGroupValidations), handler.getGroup);

//Create
router.post("/", validate(handler.createGroupValidations), handler.createGroup);

//Edit
router.put("/:groupId", validate(handler.editGroupValidations), handler.editGroup)

//Create Expense
router.post("/:groupId/expenses", validate(handler.createExpenseValidator), handler.createExpense)

//Attachment
router.post("/:groupId/expenses/:expenseId/attachment", validate(handler.createAttachmentValidations), handler.createAttachment)

//Delete Expense
router.delete("/:groupId/expense/:expenseId", validate(handler.removeExpenseValidator) ,handler.deleteExpense)

//Archive Group
router.post("/:groupId/archive", validate(handler.archiveGroupValidator), handler.archiveGroup)
//Create Payment
router.post("/:groupId/payments",validate(handler.createPaymentValidator) ,handler.createPayment)


module.exports = router