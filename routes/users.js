const express = require('express');
const router = express.Router();
const handler = require('../handlers/users')
const validator = require('../helpers/validator')

function validate(validations) {
    return [...validations, validator]
}
router.post("/register", validate(handler.registerUserValidator), handler.register);

module.exports = router