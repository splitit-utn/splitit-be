var express = require('express');
var groupsRoutes = require('./groups')
var userRoutes = require('./users')
const configs = require('../config');
var router = express.Router();

/* GET home page. */
router.use("/groups", groupsRoutes)
router.use("/users", userRoutes)
router.use('/env', (req, res) => {
    res.send(configs.name)
})

module.exports = router;