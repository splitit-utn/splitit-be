const { expect } = require('chai');
const chai = require('chai');
const sinon = require('sinon');
var sandbox = sinon.createSandbox();

//Del Proyecto
const handler = require('../handlers/groups')
const service = require('../services/GroupService')
const groupsModel = require('../models/groups')
const DebtsAlgorithmHelper = require('../services/DebtsAlgorithmHelper')

describe('Unit Test Battery', () => {
    describe('createPayment battery', () => {
        afterEach(() => sandbox.restore() )
        it('createPayment Handler', async () => {
            const req = {
                params:{groupId:'asdas87da87123sad34tr'},
                body: {
                    description: 'Descripcion de prueba',
                    amount: 100,
                    date: '2021-05-26',
                    source: { name: 'MemberSource1', id: 121 },
                    receiver: { name: 'MemberReceiver1', id: 122 },
                }
            }
            const pay = sandbox.stub(service, 'createPayment').returns(req.body)
            const res = {
                send: sandbox.spy(),
            }

            await handler.createPayment(req, res)

            //Testea si se llamo exactamente una vez el spy
            expect(res.send.calledOnce).to.be.true
            expect(res.send.args[0][0]).to.deep.equal({
                success: true,
                payment: req.body,
            })
        })

        it('createPayment Service', async () => {

            //Arrenge
            const payment = {
                description: 'Descripcion de prueba',
                amount: 100,
                date: '2021-05-26',
                source: { name: 'MemberSource1', id: 121 },
                receiver: { name: 'MemberReceiver1', id: 122 },
            }
            const group = {
                _id: 123,
                name: "Grupo de testeo de Services",
                descripcion: "Descripcion de Services",
                idAdmin: 10,
                members: [{ name: 'MemberSource1', email: 'user1@mail.com', id: 121 }, { name: 'MemberReceiver1', email: 'user2@mail.com', id: 123 }],
                payments: [],
                save: sandbox.spy(),
            }

            sandbox.stub(groupsModel, 'findById').returns(group)
            sandbox.stub(DebtsAlgorithmHelper, 'calculateTotals').returns({})
            sandbox.stub(DebtsAlgorithmHelper, 'calculateTransactions').returns([])


            //Act
            const result = await service.createPayment(payment, 123)

            //Assert
            expect(result).to.deep.equal(payment)
            expect(group.payments).to.deep.equal([payment])
            expect(group.save.calledOnce).to.be.true
        })
    })
})