const { expect } = require('chai');
const sinon = require('sinon');

//Del Proyecto
const handler = require('../handlers/groups')
const service = require('../services/GroupService')
const groupsModel = require('../models/groups')


describe('Unit Test Battery', () => {

    describe('GetGroupID battery', () => {

        beforeEach(() => {
            sandbox = sinon.createSandbox()
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('GetGroupsHandler', async () => {
            //Seteo de ambiente
            const group = {
                name: "Grupo de testeo",
                descripcion: "Descripcion",
                idAdmin: 10,
                members: [{ name: 'User1', email: 'user1@mail.com', id: 101 }]
            }
            //Como no dispongo de un return voy a tener que mandar un infiltrado a ver que pasa
            let json = sandbox.spy()
            let res = { json }
            const req = { params: { groupId: 'asv1538654asf' } }

            sandbox.stub(service, 'getGroup').returns(group)

            //Ejecuto la funcion para que el spy recopile datos
            await handler.getGroup(req, res)

            //Pruebas
            //Corrobora si se llamo el metodo alguna ves
            expect(json.calledOnce).to.be.true;
            //Se puede chequear de las dos formas
            expect(json.args[0][0]).to.deep.eq(group)

            expect(json.args[0][0]).have.property('descripcion').eq('Descripcion')
            expect(json.args[0][0]).have.property('name').eq('Grupo de testeo')
            expect(json.args[0][0].members[0]).have.property('name').eq('User1')
        })

        it('GetGroupsService', async () => {
            //Arrenge
            const group = {
                name: "Grupo de testeo de Services",
                descripcion: "Descripcion de Services",
                idAdmin: 10,
                members: [{ name: 'User1', email: 'user1@mail.com', id: 101 }]
            }
            sandbox.stub(groupsModel, 'findById').returns(group)

            //Act
            const resultado = await service.getGroup('asigojas')

            //Assert
            expect(resultado).have.property('name').eq("Grupo de testeo de Services")

        })
    })

})