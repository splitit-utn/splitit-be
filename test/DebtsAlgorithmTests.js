const { expect } = require('chai');
const sinon = require('sinon');
const sandbox = sinon.createSandbox()

const handler = require('../handlers/groups')
const service = require('../services/GroupService')
const groupsModel = require('../models/groups')
const UserService = require('../services/UserService')
const httpContext = require('express-http-context');
const groupSchema = require('../models/groups')

const DebtsAlgorithmHelper = require('../services/DebtsAlgorithmHelper')

describe('Unit tests de algoritmo de deudas', () => {
    const juan = {
        _id: '123',
        name: 'Juan'
    }
    const santiago = {
        _id: '456',
        name: 'Santiago'
    }
    const esteban = {
        _id: '789',
        name: 'Quito'
    }
    var group = undefined
    beforeEach(() => {
        group = {
            _id: 'qwe',
            members: [juan, santiago, esteban],
            expenses: []
        }
        sandbox.restore()
        sandbox.stub(UserService, 'SendNotification')
    })
    after(() => sandbox.restore())
    describe('Calcular Total tests', () => {
        it('Deuda simple', () => {
            group.expenses.push({
                paidBy: juan,
                consumeBy: [juan, santiago],
                amount: 2000
            })
            group.members = [juan, santiago]

            const result = DebtsAlgorithmHelper.calculateTotals(group)

            expect(result).to.have.lengthOf(2);
            expect(result.some(x => x.member == santiago && x.total == -1000)).to.be.true;
            expect(result.some(x => x.member == juan && x.total == 1000)).to.be.true;
        })
        it('Deuda cancelada', () => {
            group.expenses.push({
                paidBy: juan,
                consumeBy: [juan, santiago],
                amount: 2000
            })
            group.expenses.push({
                paidBy: santiago,
                consumeBy: [juan, santiago],
                amount: 2000
            })
            group.members = [juan, santiago]

            const result = DebtsAlgorithmHelper.calculateTotals(group)

            expect(result).to.have.lengthOf(2);
            expect(result.some(x => x.member == santiago && x.total == 0)).to.be.true;
            expect(result.some(x => x.member == juan && x.total == 0)).to.be.true;
        })
        it('Deuda triple', () => {
            group.expenses.push({
                paidBy: juan,
                consumeBy: [juan, santiago, esteban],
                amount: 3000
            })
            group.expenses.push({
                paidBy: esteban,
                consumeBy: [juan],
                amount: 1000
            })

            const result = DebtsAlgorithmHelper.calculateTotals(group)

            expect(result).to.have.lengthOf(3);
            expect(result.some(x => x.member == santiago && x.total == -1000)).to.be.true;
            expect(result.some(x => x.member == juan && x.total == 1000)).to.be.true;
            expect(result.some(x => x.member == esteban && x.total == 0)).to.be.true;
        })
        it('Deuda triple 2', () => {
            group.expenses.push({
                paidBy: juan,
                consumeBy: [juan, santiago, esteban],
                amount: 3000
            })
            group.expenses.push({
                paidBy: esteban,
                consumeBy: [santiago, esteban],
                amount: 1000
            })

            const result = DebtsAlgorithmHelper.calculateTotals(group)

            expect(result).to.have.lengthOf(3);
            expect(result.some(x => x.member == santiago && x.total == -1500)).to.be.true;
            expect(result.some(x => x.member == juan && x.total == 2000)).to.be.true;
            expect(result.some(x => x.member == esteban && x.total == -500)).to.be.true;
        })
        it('Deuda flotante', () => {
            group.expenses.push({
                paidBy: juan,
                consumeBy: [juan, santiago, esteban],
                amount: 1000
            })

            const result = DebtsAlgorithmHelper.calculateTotals(group)

            expect(result).to.have.lengthOf(3);
            expect(result.some(x => x.member == santiago && x.total == -333.33)).to.be.true;
            expect(result.some(x => x.member == juan && x.total == 666.67)).to.be.true;
            expect(result.some(x => x.member == esteban && x.total == -333.33)).to.be.true;
        })

    })

    describe('Calcular transacciones tests', () => {
        it('Deuda simple', () => {
            const totals = [
                { member: juan, total: 1000 },
                { member: santiago, total: -1000 },
            ]

            const result = DebtsAlgorithmHelper.calculateTransactions(totals)

            expect(result).to.have.lengthOf(1)
            expect(result[0].amount).to.be.eq(1000)
            expect(result[0].debtor).to.be.eq(santiago)
            expect(result[0].creditor).to.be.eq(juan)
        })
        it('Deuda triple', () => {
            const totals = [
                { member: juan, total: 1500 },
                { member: santiago, total: -1000 },
                { member: esteban, total: -500 },
            ]

            const result = DebtsAlgorithmHelper.calculateTransactions(totals)

            expect(result).to.have.lengthOf(2)

            const santJuan = result.find(x => x.debtor == santiago && x.creditor == juan)
            expect(santJuan).to.not.be.null
            expect(santJuan.amount).to.be.eq(1000)

            const estJuan = result.find(x => x.debtor == esteban && x.creditor == juan)
            expect(estJuan).to.not.be.null
            expect(estJuan.amount).to.be.eq(500)
        })
        it('2 Acreedores', () => {
            const totals = [
                { member: juan, total: 1000 },
                { member: santiago, total: 2000 },
                { member: esteban, total: -3000 },
            ]

            const result = DebtsAlgorithmHelper.calculateTransactions(totals)

            expect(result).to.have.lengthOf(2)

            const santJuan = result.find(x => x.debtor == esteban && x.creditor == juan)
            expect(santJuan).to.not.be.null
            expect(santJuan.amount).to.be.eq(1000)

            const estJuan = result.find(x => x.debtor == esteban && x.creditor == santiago)
            expect(estJuan).to.not.be.null
            expect(estJuan.amount).to.be.eq(2000)
        })

        it('Issue n1', () => {
            const payload = [
                {
                    member: juan,
                    total: 3333.33
                },
                {
                    member: santiago,
                    total: -1666.67
                },
                {
                    member: esteban,
                    total: -1666.67
                }
            ]

            const result = DebtsAlgorithmHelper.calculateTransactions(payload)

            expect(result).to.have.lengthOf(2)

            const santJuan = result.find(x => x.debtor == santiago && x.creditor == juan)
            expect(santJuan).to.not.be.null
            expect(santJuan.amount).to.be.eq(1666.67)

            const estJuan = result.find(x => x.debtor == esteban && x.creditor == juan)
            expect(estJuan).to.not.be.null
            expect(estJuan.amount).to.be.eq(1666.66)
        })
    })

    describe('Llamada desde el service', () => {
        it('Llamada al actualizar', async () => {
            const group = {
                expenses: [],
                payments: [],
                save: () => { }
            }
            const summary = [
                {
                    debtor: juan,
                    creditor: santiago,
                    amount: 1000
                }
            ]
            sandbox.stub(groupsModel, 'findById').returns(group)
            const totalsSpy = sandbox.stub(DebtsAlgorithmHelper, 'calculateTotals').returns({})
            const transSpy = sandbox.stub(DebtsAlgorithmHelper, 'calculateTransactions').returns(summary)

            await service.createExpense({ consumeBy: [] }, 'asdf')

            expect(group.summary).to.deep.eq(summary)
            expect(totalsSpy.calledOnce).to.be.true
            expect(transSpy.calledOnce).to.be.true
        })
        it('Llamada al eliminar gasto', async () => {
            const group = {
                expenses: [],
                payments: [],
                save: () => { }
            }
            const summary = [
                {
                    debtor: juan,
                    creditor: santiago,
                    amount: 1000
                }
            ]
            sandbox.stub(groupsModel, 'findById').returns(group)
            const totalsSpy = sandbox.stub(DebtsAlgorithmHelper, 'calculateTotals').returns({})
            const transSpy = sandbox.stub(DebtsAlgorithmHelper, 'calculateTransactions').returns(summary)

            await service.deleteAnExpense('qwer', 'asdf')

            expect(group.summary).to.deep.eq(summary)
            expect(totalsSpy.calledOnce).to.be.true
            expect(transSpy.calledOnce).to.be.true
        })
    })
})