const { expect } = require('chai');
const sinon = require('sinon');

//Del Proyecto
const service = require('../services/GroupService')
const groupsModel = require('../models/groups')
const UserService = require('../services/UserService')
const emailService = require('../services/emailService')

describe('EditGroup UnitTest', () => {

    beforeEach(() => {
        sandbox = sinon.createSandbox()
    })

    afterEach(() => {
        sandbox.restore()
    })

    it('editGroup Service', async () => {
        //Arrenge
        const membersGroup = [{ name: 'Chefsito', email: 'marcelo@hotmail.com' },
        { name: 'userNotRegistered', email: 'mail@hotmail.com' },
        { name: 'Josefina', email: 'josefina@hotmail.com' }]
        const saveGroupSpy = sandbox.spy()
        const group = { _id: '123abc', name: 'Test', members: membersGroup, save: saveGroupSpy }

        //1 user notDeleted (Josefina)
        //1 new user that is registered (Chefsito)
        //1 new user that isn't registered (userNotRegistered)
        //1 deleted user (Pedro)

        const membersDbGroup = [{ name: 'Pedro', email: 'alfonso@mail.com' },
        { name: 'Josefina', email: 'josefina@hotmail.com' },
        { name: 'currentUser', email: 'currentUser@hotmail.com' }]

        const groupDb = { _id: '123abc', name: 'Test Group', members: membersDbGroup, save: sandbox.spy() }

        const userTest1 = {
            _id: 'checkID1',
            name: 'Josefina',
            save: sandbox.spy(),
            memberOf: [{ id: '111', name: 'AnotherGroup1' }, { id: '123abc', name: 'Test Group' }]
        }

        const userTest2 = {
            _id: 'checkID2',
            name: 'Marcelo',
            save: sandbox.spy(),
            memberOf: [{ id: '111', name: 'AnotherGroup2' }]
        }

        const userTest3 = {
            _id: 'checkID3',
            name: 'Test',
            save: sandbox.spy(),
            memberOf: [{ id: '111', name: 'AnotherGroup3' }, { id: '123abc', name: 'Test Group' }]
        }

        sandbox.stub(groupsModel, 'findById').returns(groupDb)
        sandbox.stub(UserService, 'getUserByEmail').withArgs('marcelo@hotmail.com').returns(userTest2)
            .withArgs('josefina@hotmail.com').returns(userTest1)
            .withArgs('currentUser@hotmail.com').returns(userTest3)
        sandbox.stub(groupsModel.prototype, 'save')
        const spyEmail = sandbox.stub(emailService, 'sendInvitation')

        //Act
        await service.editGroup(group)

        //Assert
        expect(userTest1.memberOf.some(x => x.name == 'Test')).to.be.true
        expect(userTest1.save.calledOnce).to.be.true

        expect(userTest3.memberOf.some(x => x.name == 'Test')).to.be.true
        expect(userTest3.save.calledOnce).to.be.true

        expect(userTest2.memberOf.some(x => x.name == 'Test')).to.be.true
        expect(userTest2.save.calledOnce).to.be.true

        expect(spyEmail.calledOnce).to.be.true
        expect(spyEmail.args[0][0]).to.be.eq('mail@hotmail.com')
        expect(groupDb.members[groupDb.members.length - 1]).to.deep.eq({ name: 'currentUser', email: 'currentUser@hotmail.com' })
        expect(groupDb.members.length).to.be.eq(4)
        expect(groupDb.name).to.be.eq('Test')
        expect(groupDb.members.some(x => x.name == 'Marcelo' && x.email == 'marcelo@hotmail.com')).to.be.true
        expect(groupDb.members.some(x => x.name == 'currentUser' && x.email == 'currentUser@hotmail.com')).to.be.true
        expect(groupDb.members.some(x => x.name == 'Josefina' && x.email == 'josefina@hotmail.com')).to.be.true
        expect(groupDb.members.some(x => x.name == 'Pedro' && x.email == 'alfonso@mail.com')).to.be.false
        expect(groupDb.save.calledTwice).to.be.true
    })
})