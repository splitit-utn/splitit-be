const { expect } = require('chai');
const sinon = require('sinon');
var sandbox = sinon.createSandbox();


//Del Proyecto

const handler = require('../handlers/groups')
const groupService = require('../services/GroupService')
const userService = require('../services/UserService')
const httpContext = require('express-http-context');

describe('Unit Test Battery', () => {
    describe('archiveGroup battery', () => {
        afterEach(() => sandbox.restore() )
        it('archiveGroup Handler', async () => {
            const req = {
                params:{groupId:'asdas87da87123sad34tr'},
            }
            const user = {
                memberOf: [
                    {
                        name: "Grupo archivado test",
                    }
                ]
            }
            sandbox.stub(groupService, 'archiveGroup').returns(true)
            const res = {
                send: sandbox.spy(),
            }


            await handler.archiveGroup(req, res)


            expect(res.send.calledOnce).to.be.true
            expect(res.send.args[0][0]).to.deep.equal({
                success: true
            })

        })


        it('archiveGroup Service', async () => {
            const user = {
                memberOf: [
                    {
                        name: "Grupo archivado test",
                        archived: false,
                        id: '123'
                    }
                ],
                save: sandbox.spy(() => user)
            }

            sandbox.stub(httpContext, 'get').returns(user)
            sandbox.stub(userService, 'GetUserOrCreate').withArgs(user).returns(user)

            const result = await groupService.archiveGroup('123')

            expect(user.memberOf[0]).have.property('name').eq("Grupo archivado test")
            expect(user.memberOf[0].archived).to.be.true
            expect(user.save.calledOnce).to.be.true
        })

    })

})