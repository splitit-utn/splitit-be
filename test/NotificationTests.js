const { expect } = require('chai');
const sinon = require('sinon');
const sandbox = sinon.createSandbox()

const handler = require('../handlers/groups')
const service = require('../services/UserService')
const userModel = require('../models/users')
const UserService = require('../services/UserService')
const httpContext = require('express-http-context');
const groupSchema = require('../models/groups')
const Sentry = require("@sentry/node");
const request = require('request');


describe('Unit tests de Notificaciones', () => {
    beforeEach(() => {
        sandbox.restore()
    })
    after(() => sandbox.restore())

    describe('Register User', async () => {
        it('Registrar usuario', async () => {
            const userDb = {
                email: 'mail@test.com',
                save: sinon.spy()
            }
            const contextUser = {
                name: 'Context User',
                email: 'mail@test.com',
            }
            sandbox.stub(userModel, 'find').returns([])
            sandbox.stub(httpContext, 'get').returns(contextUser)
            sandbox.stub(userModel, 'findOne').withArgs({ email: contextUser.email }).returns(userDb)

            const expoToken = 'testToken'

            await service.RegisterUser(expoToken)

            expect(userDb.expoToken).to.be.eq(expoToken)
            expect(userDb.save.calledOnce).to.be.true
        })

        it('Registrar nuevo usuario', async () => {
            const createdUser = {
                name: 'Catarsis',
                email: 'mail@test.com',
                save: sinon.spy()
            }
            const contextUser = {
                name: 'Context User',
                email: 'mail@test.com',
                googleId: 'adsf'
            }
            sandbox.stub(httpContext, 'get').returns(contextUser)
            sandbox.stub(userModel, 'findOne').returns(null)
            sandbox.stub(userModel, 'find').returns([])
            sandbox.stub(userModel, 'create').returns(createdUser)

            const expoToken = 'testToken'

            await service.RegisterUser(expoToken)

            expect(createdUser.expoToken).to.be.eq(expoToken)
            expect(createdUser.save.calledOnce).to.be.true
        })

        it('Error al registrar', async () => {
            const contextUser = {
                name: 'Context User',
                email: 'mail@test.com',
            }
            sandbox.stub(httpContext, 'get').returns(contextUser)
            sandbox.stub(userModel, 'find').returns([])
            sandbox.stub(userModel, 'findOne').throws(new Error('Test Error'));
            const sentryStub = sandbox.stub(Sentry, 'captureException');

            const expoToken = 'testToken'
            let error = null
            try {
                await service.RegisterUser(expoToken)
            } catch (err) {
                error = err
            }
            expect(error).not.to.be.null
            expect(sentryStub.calledOnce).to.be.true
        })
    })


    describe('Send Notifications', () => {
        it.skip('Enviar notificacion', async () => {
            const requestStub = sandbox.stub(request, 'post')
            const user = {
                email: 'mail@test.com',
                expoToken: 'expoToken'
            }
            const content = 'test contet'

            await service.SendNotification(user, content)

            expect(requestStub.calledOnce).to.be.true
            expect(requestStub.args[0][0].headers).not.to.be.null
            expect(requestStub.args[0][0].url).to.be.eq('https://exp.host/--/api/v2/push/send')
            expect(requestStub.args[0][0].body.to).to.be.eq(user.expoToken)
            expect(requestStub.args[0][0].body.body).to.be.eq(content)
        })
        it.skip('Error Enviar notificacion', async () => {
            const requestStub = sandbox.stub(request, 'post').throws(new Error('something'))
            const sentryStub = sandbox.stub(Sentry, 'captureException');

            const user = {
                email: 'mail@test.com',
                expoToken: 'expoToken'
            }
            const content = 'test contet'

            await service.SendNotification(user, content)

            expect(sentryStub.calledOnce).to.be.true
            expect(requestStub.calledOnce).to.be.true
        })
    })

})