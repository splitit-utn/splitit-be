const { expect } = require('chai');
const sinon = require('sinon');

//Del Proyecto
const handler = require('../handlers/groups')
const service = require('../services/GroupService')
const groupsModel = require('../models/groups')
const UserService = require('../services/UserService')
const httpContext = require('express-http-context');
const emailService = require('../services/emailService')

describe('createGroup battery', () => {

    beforeEach(() => {
        sandbox = sinon.createSandbox()
        sandbox.stub(UserService, 'SendNotification')
    })

    afterEach(() => {
        sandbox.restore()
    })
    it('createGroup Handler', async () => {
        const reqGroup = {
            body: {
                name: 'Name Group Test',
                description: 'Description test',
                members: [{ name: 'MemberTest1', mail: 'test@mail.com', id: 12 }],
                idAdmin: 10,
            }
        }
        sandbox.stub(service, 'createGroup').returns(reqGroup)
        const json = sandbox.spy()
        const res = { json }

        await handler.createGroup(reqGroup, res)

        //Testea si se llamo exactamente una vez el spy
        expect(json.calledOnce).to.be.true
        expect(json.args[0][0]).to.deep.eq({
            success: true,
            group: reqGroup,
        })

    })

    it('createGroup Service', async () => {
        //Arrenge
        const group = {
            name: 'Name Group Test',
            description: 'Description test',
            members: [{ name: 'MemberTest1', email: 'test@mail.com' },
            { name: 'MemberTest2', email: 'test2@mail.com' },
            { name: 'MemberNotRegistred', email: 'test3@mail.com' }],
            idAdmin: 10,
        }

        const contextStub = sandbox.stub(httpContext, 'get').returns('testMail@mail.com')
        const userTest1 = { _id: '123', name: 'UserTest1', memberOf: [], save: sandbox.spy() }
        const userTest2 = { name: 'UserTest2', memberOf: [], save: sandbox.spy() }

        const GetUserOrCreateStub = sandbox.stub(UserService, 'GetUserOrCreate').withArgs('testMail@mail.com').returns(userTest1)
        sandbox.stub(UserService, 'getUserByEmail')
            .withArgs('test2@mail.com').returns(userTest2)
            .withArgs('test@mail.com').returns(userTest1)

        const gr = sandbox.stub(groupsModel.prototype, 'save')
        const spyEmai = sandbox.stub(emailService, 'sendInvitation')
        //const spySendInvitacion = sandbox.spy(emailService, 'sendInvitation')

        //Act
        const result = await service.createGroup(group)

        //Assert
        const newGroup = result.newGroup;

        expect(contextStub.called).to.be.true
        expect(GetUserOrCreateStub.calledOnce).to.be.true
        expect(newGroup.members.some(x => x.name == userTest1.name)).to.be.true
        expect(spyEmai.args[0][0]).to.be.eq('test3@mail.com')
        expect(userTest1.memberOf.some(x => x.id == newGroup._id && x.name == newGroup.name)).to.be.true
        expect(userTest1.save.calledOnce).to.be.true
        expect(userTest2.memberOf.some(x => x.id == newGroup._id && x.name == newGroup.name)).to.be.true
        expect(userTest2.save.calledOnce).to.be.true
        expect(gr.callCount).to.be.eq(2)
    })
})