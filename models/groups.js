const { Schema, model, Types } = require('mongoose')

const userRefSchema = new Schema({
    name: String,
    email: String
})

const SummaryItem = new Schema({
    debtor: userRefSchema,
    creditor: userRefSchema,
    amount: Number
}, { _id: false })

const paymentSchema = new Schema({
    description: String,
    amount: Number,
    date: String,
    source: userRefSchema,
    receiver: userRefSchema
})

const expenseSchema = new Schema({
    description: String,
    amount: Number,
    date: String,
    attachments: [Types.ObjectId],
    paidBy: userRefSchema,
    consumeBy: [userRefSchema]
})

const groupSchema = new Schema({
    name: String,
    description: String,
    idAdmin: Number,
    members: [userRefSchema],
    expenses: [expenseSchema],
    payments: [paymentSchema],
    summary: [SummaryItem]
});

module.exports = model('Group', groupSchema)