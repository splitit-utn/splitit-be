const { Schema, model } = require('mongoose')

const attachmentSchema = new Schema({
    img: Buffer
});

module.exports = model('Attachment', attachmentSchema)