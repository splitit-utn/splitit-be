const { Schema, model, Types } = require('mongoose')

const groupRefSchema = new Schema({
    id: Types.ObjectId,
    name: String,
    archived: {
        type: Boolean,
        default: false
    }
}, { _id: false });

const usersSchema = new Schema({
    name: String,
    googleId: String,
    email: String,
    expoToken: String,
    memberOf: [groupRefSchema],
});

module.exports = model('User', usersSchema)