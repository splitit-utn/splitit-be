const jwt = require('jsonwebtoken');
const request = require('request');
const httpContext = require('express-http-context');

var google_certs = {}

const googleClientIds = [
    '212634148532-i2a5v1laifenokkbf6v95obot98p546e.apps.googleusercontent.com',
    '212634148532-f9kc4mvluttd0vucrniro1pilpooj9ro.apps.googleusercontent.com'
]

const default_user = {
    name: 'Guillermo Marcel Test',
    email: 'guille.marcel04@test.com',
    id: '608db8d8586c5c8cf562cf92',
    googleId: '111834809546349713634'
}

const test_user = {
    name: 'Test User Splitit',
    email: 'testuser@splitit.com',
    id: undefined,
    googleId: undefined
}

function reload_certificates() {
    request.get({
        url: 'https://www.googleapis.com/oauth2/v1/certs',
        json: true
    }, function (err, resp, certs) {
        google_certs = certs
    });
}
function setCerstLoader() {
    setTimeout(function () {
        reload_certificates();
    }, 24 * 60 * 60); // since Google said that can change daily
}

const GoogleAuthenticator = (req) => {
    var auth_header = req.get('Authorization');

    var token = auth_header.replace(/^Bearer\s/, '');

    var token_header;

    try {
        token_header = JSON.parse(Buffer.from(token.split('.')[0], 'base64').toString());
    } catch (err) {
        console.log("can't parse the JWT's header");
        return null
    }

    const key_id = token_header.kid;
    const cert = google_certs[key_id]

    try {
        var payload = jwt.verify(token, cert, {
            algorithms: ['RS256'],
            audience: googleClientIds,
            issuer: 'https://accounts.google.com',
            ignoreExpiration: true
        })
        return {
            name: payload.name,
            email: payload.email,
            id: undefined,
            googleId: payload.sub
        }
    } catch (er) {
        console.log(er)
        return null
    }
}

const TestUserAuth = (req) => {
    var auth_header = req.get('Authorization');
    if (auth_header.includes('testtoken'))
        return test_user
    return null
}

module.exports = () => {

    setCerstLoader()

    reload_certificates();

    return function (req, res, next) {
        var auth_header = req.get('Authorization');

        let user = null

        if (!auth_header) {
            user = default_user
        }

        if (!user)
            user = TestUserAuth(req)

        if (!user)
            user = GoogleAuthenticator(req)

        if (user) {
            req.user = user
            httpContext.set('user', user)
            return next()
        }

        res.status(401).send()
        return false
    }
}